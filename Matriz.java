package principal;


import java.text.DecimalFormat;
import java.util.LinkedList;


import java.util.HashSet;
import java.util.Scanner;


public class Matriz {

	private int fila, columna;
	private LinkedList<LinkedList<Integer>> matriz = new LinkedList<LinkedList<Integer>>();
	private Scanner sc;
	
	Matriz(){
		sc = new Scanner(System.in);
		System.out.println("Ingrese fila - columna");
		fila    = sc.nextInt();
		columna = sc.nextInt();
	
		for(int i=0; i < fila; i++){
			matriz.add(new LinkedList<Integer>());
			for(int j=0; j < columna; j++){			
				matriz.get(i).add(sc.nextInt());
			}
		}		
	}
	    
	public void mostrar(){
		System.out.println(matriz);
	}
		
	public void dominacion(){
		LinkedList<Integer> Alm = new LinkedList<Integer>();
		int cont_t = 0;
		int cont_i = 0;
		for(int i=0; i < fila; i++){
			for(int t = 1 + i; t < fila; t++){
				cont_t= 0; cont_i = 0;
				for(int j = 0; j < columna; j++){
					if(matriz.get(i).get(j) >= matriz.get(t).get(j)) cont_t++;
					if(matriz.get(i).get(j) <= matriz.get(t).get(j)) cont_i++;
				}
				if(cont_t == fila){					
					Alm.add(t);
				}
				if(cont_i == fila){					
					Alm.add(i);
				}
			}
		}
		
		HashSet<Integer> existe = new HashSet<Integer>(); 
		for(Integer ls : Alm){
				if(existe.add(ls)){
					matriz.remove((int) ls);
					fila--;
				}					
		}		
	}
	
	public void maxmin(){
		Integer[] min = new Integer[fila];
		for(int i=0; i < fila; i++) min[i] = Integer.MAX_VALUE;
		for(int i=0; i < fila; i++){
			for(int j=0; j < columna; j++){
				if(min[i] >= matriz.get(i).get(j)) min[i] = matriz.get(i).get(j);
			}
		}
		Integer max =  Integer.MIN_VALUE;
		int pos = 0;
		for(int i=0; i < fila; i++){
			if(max <= min[i]){
				max = min[i];
				pos = i;
			}
		}
		mostrar(matriz.get(pos),pos);
	}
	public void maxmax(){
		Integer[] max = new Integer[fila];
		for(int i=0; i < fila; i++) max[i] = Integer.MIN_VALUE;
		for(int i=0; i < fila; i++){
			for(int j=0; j < columna; j++){
				if(max[i] <= matriz.get(i).get(j)) max[i] = matriz.get(i).get(j);
			}
		}
		Integer maximo =  Integer.MIN_VALUE;
		int pos = 0;
		for(int i=0; i < fila; i++){
			if(maximo <= max[i]){
				maximo = max[i];
				pos = i;
			}
		}
		mostrar(matriz.get(pos),pos);
	}
	
	void mostrar(LinkedList<Integer> n,int pos){		
		System.out.println("La Alternativa es a" + (pos+1) + " : ");
		for(int i=1; i <= columna; i++) System.out.print("Q"+i+"\t");
		System.out.println();
		for(Integer ls: n){			
			System.out.print(ls + "\t");
		}
		System.out.println();
	}
	
	void Hurwicz(double d){
		double[][] estados = new double[fila][2];
		Double[] Hurwicz = new Double[fila];
		for(int i=0; i < fila; i++) {
			estados[i][0] = Double.MIN_VALUE; // Maximo 
			estados[i][1] = Double.MAX_VALUE; // Minimo
		}
		for(int i=0; i < fila; i++){
			for(int j=0; j < columna; j++){
				if(estados[i][0] <= matriz.get(i).get(j)) estados[i][0] = (double) matriz.get(i).get(j);
				if(estados[i][1] >= matriz.get(i).get(j)) estados[i][1] = (double) matriz.get(i).get(j);				
			}
		}
		for(int i=0; i < fila; i++) Hurwicz[i] = estados[i][1]*d + (1 - d)*estados[i][0];
		double n = Double.MIN_VALUE; //minimo.
		Integer pos = 0;
		for(int i=0; i < fila; i++) if(n <= Hurwicz[i]){ n = Hurwicz[i]; pos = i;}
		DecimalFormat df = new DecimalFormat("#.#");
		
		for(int i=0; i < fila; i++) System.out.println(df.format(Hurwicz[i]));
		mostrar(matriz.get(pos),pos);
		
	}


}
