Universidad de Oriente.
Proyecto Toma de Decisiones.

Realización de la Matriz de Toma de Decisiones.
 - Se realizó matriz dinámica, basada en LinkedList de Java.
 - Método de Dominación (Se elimina dinámicamente la fila correspondiente)
 - Método MiniMax
 - Método MaxiMax
 - Método Hurwicz [alfa*Cmin + (1-alfa)*Cmax]
 - Método de Arrepentimiento ( Matriz de Pesar, Max Pesar) -> Por Desarrollar.
 - Método de Laplace. -> Por Desarrollar.
 - Método V.E ( Método del Valor Esperado) - Por Desarrollar.